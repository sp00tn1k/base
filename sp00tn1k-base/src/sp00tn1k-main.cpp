/**
 * sp00tn1k-base
 * Rudimentary driver code for sp00tn1k telepresence robot.
 * Individual PID for each motor.
 * Board is Arduino Mega 2560
 * Motor driver Cytron MDD10A
 */


// Build Parameters
#define VERSION	0.4b
// #define DEV_BUILD

#include <Arduino.h>

#include <ros.h>
#include <ros/time.h>
#include <geometry_msgs/Twist.h>

// ROS communication
static ros::NodeHandle nh;

//#include "sp00tn1k-battery.hpp"
#include "sp00tn1k-drive-base.hpp"
// #include "sp00tn1k-imu.hpp"
#include "sp00tn1k-uv-c.hpp"

/************* globals ********************************/

DriveBase base;
// IMU imu;
UVCLamp uvc;

// ros subscriber must be defined globally, therefore this trick
void twistMessageCallback(const geometry_msgs::Twist &msg) {
    base.twistMessageCallback(msg);
}
ros::Subscriber<geometry_msgs::Twist> twist_sub(base.getDrivingTopic(), &twistMessageCallback);

void pidMessageCallback(const std_msgs::Float32MultiArray &msg){
    base.motorsPIDCallback(msg);
}
ros::Subscriber<std_msgs::Float32MultiArray> pid_sub("motors_pid", &pidMessageCallback);

volatile unsigned long enc_a_ts = 0;
volatile unsigned long enc_b_ts = 0;
const unsigned long noise_time = 200; // micros

/************* setup ********************************/
void setup() {

    // WARNING!!!
    // This line was fount to cause phantom ISR ticks!
    // TCCR2B = TCCR2B & 0b11111000 | 0x01;

    ArduinoHardware *hw = nh.getHardware();
    hw->setBaud(500000);

    nh.initNode();

    nh.subscribe(twist_sub);
    nh.subscribe(pid_sub);

    // attach interrupts to motors
    // this uglyness is here because ISR has to be set from the global scope
    // right
    attachInterrupt(
        digitalPinToInterrupt(ENC_A1_A), [] {
            unsigned long now = micros();
            if (now - enc_a_ts > noise_time) {
                if(digitalRead(ENC_A1_A) == HIGH) {
                    (digitalRead(ENC_A1_B) == HIGH) ? base.motor1.encoderTick() : base.motor1.encoderTock();
                }   
            }
            enc_a_ts = now;
        },
        CHANGE);
    // left
    attachInterrupt(
        digitalPinToInterrupt(ENC_A2_A), [] {
            unsigned long now = micros();
            if (now - enc_b_ts > noise_time) {
                if(digitalRead(ENC_A2_A) == HIGH) {
                    (digitalRead(ENC_A2_B) == HIGH) ? base.motor2.encoderTock() : base.motor2.encoderTick();
                }   
            }
            enc_b_ts = now;
        },
        CHANGE);


    nh.loginfo("[sp00tn1k-base] Ready.");

}

/************* loop ********************************/
void loop() {

    // Keep ROS spinning
    if (nh.spinOnce() != ros::SPIN_OK) {
        // TODO stop motors if not connected to ros



    } else {
    
        // update imu to get fresh data published to ROS
        // TODO: move this out of if clause if we are going to do some rudimentary autonomy with dead reconing when the connection to host ROS is lost
        // imu.update();

    }

    // call motor update to keep them computing and spinning
    base.update();

}
