#ifdef UNIT_TEST

#include <unity.h>

#include "sp00tn1k-planner.hpp"

// test first element placement
void FirstPointCurrentSpeedInRange() {
    const double lowLim = -50.0;
    const double highLim = 30.0;

    MotionPlanner* planner = new MotionPlanner(lowLim, highLim, 64, 4000000);
    Plan* plan;
    // this should be in range
    planner->fillPlan(plan, lowLim + 1, highLim);
    TEST_ASSERT_EQUAL(plan->pop()->value, lowLim + 1);

    planner->fillPlan(plan, highLim - 1, highLim);
    TEST_ASSERT_EQUAL(plan->pop()->value, highLim - 1);

    // this should top limit
    planner->fillPlan(plan, highLim + 1, highLim);
    TEST_ASSERT_EQUAL(plan->pop()->value, highLim);

    // this should be low limit
    planner->fillPlan(plan, lowLim - 1, highLim);
    TEST_ASSERT_EQUAL(plan->pop()->value, lowLim);

    delete planner;
}

// test last element placement
// void LastPointCurrentSpeedInRange() {
//     const double lowLim = -50.0;
//     const double highLim = 30.0;

//     MotionPlanner* planner = new MotionPlanner(lowLim, highLim, 64, 4.0f);
//     Plan* plan;
//     // this should be in range
//     planner->fillPlan(lowLim, highLim - 1);
//     PlanPoint *last_p, *p = plan->pop();
//     while (p != nullptr) {
//         last_p = p;
//         p = plan->pop();
//     }
//     TEST_ASSERT_EQUAL(last_p->value, highLim - 1);

//     planner->fillPlan(highLim, lowLim + 1);
//     p = plan->pop();
//     while (p != nullptr) {
//         last_p = p;
//         p = plan->pop();
//     }
//     TEST_ASSERT_EQUAL(last_p->value, lowLim + 1);
//     // this should hit the limits
//     planner->fillPlan(highLim, lowLim - 1);
//     p = plan->pop();
//     while (p != nullptr) {
//         last_p = p;
//         p = plan->pop();
//     }
//     TEST_ASSERT_EQUAL(last_p->value, lowLim);

//     planner->fillPlan(lowLim, highLim + 1);
//     p = plan->pop();
//     while (p != nullptr) {
//         last_p = p;
//         p = plan->pop();
//     }
//     TEST_ASSERT_EQUAL(last_p->value, highLim);

//     delete planner;
// }

// // positive slope
// void PositiveSlope() {
//     MotionPlanner* planner = new MotionPlanner(-300, 300, 64, 4.0f);
//     Plan* planner->fillPlan(-400, 400);

//     PlanPoint *last_p, *p = plan->pop();
//     last_p = p;
//     p = plan->pop();

//     while (p != nullptr) {
//         TEST_ASSERT_TRUE(last_p->value < p->value);
//         TEST_ASSERT_TRUE(p->timestep > 0);
//         last_p = p;
//         p = plan->pop();
//     }

//     delete planner;
// }

// void NegativeSlope() {
//     MotionPlanner* planner = new MotionPlanner(-300, 300, 64, 4.0f);
//     Plan* planner->fillPlan(400, -400);

//     PlanPoint *last_p, *p = plan->pop();
//     last_p = p;
//     p = plan->pop();

//     while (p != nullptr) {
//         TEST_ASSERT_TRUE(last_p->value > p->value);
//         TEST_ASSERT_TRUE(p->timestep > 0);
//         last_p = p;
//         p = plan->pop();
//     }

//     delete planner;
// }

// void DeletePlan(){
//     MotionPlanner* planner = new MotionPlanner(-300, 300, 64, 4.0f);
//     Plan* planner->fillPlan(400, -400);

//     delete plan;
//     delete planner;

//     TEST_ASSERT_TRUE(plan->isEmpty());
// }

#endif