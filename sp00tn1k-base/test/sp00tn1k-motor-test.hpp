#ifdef UNIT_TEST

#include <unity.h>

#include "sp00tn1k-motor.hpp"
#include "sp00tn1k-hardware.h"

void CreateMotors(){

    Motor motor1 = Motor("r", PWM_DIR, MOTOR_A1_PWM, MOTOR_A1_DIR, ENC_A1_PWR, ENC_A1_A, ENC_A1_B);  // MOTOR1, assumed Left
    Motor motor2 = Motor("l", PWM_DIR, MOTOR_A2_PWM, MOTOR_A2_DIR, ENC_A2_PWR, ENC_A2_A, ENC_A2_B);  // MOTOR2, assumed Right

    TEST_ASSERT_EQUAL_INT(0, motor1.update());
    TEST_ASSERT_EQUAL_INT(0, motor2.update());

}

void CheckEncoders(){
    Motor motor1 = Motor("r", PWM_DIR, MOTOR_A1_PWM, MOTOR_A1_DIR, ENC_A1_PWR, ENC_A1_A, ENC_A1_B);  // MOTOR1, assumed Left
    Motor motor2 = Motor("l", PWM_DIR, MOTOR_A2_PWM, MOTOR_A2_DIR, ENC_A2_PWR, ENC_A2_A, ENC_A2_B);  // MOTOR2, assumed Right

    TEST_ASSERT_EQUAL_INT(0, motor1.getEncoder());
    TEST_ASSERT_EQUAL_INT(0, motor2.getEncoder());

    for (int i = 0; i < 1000; i++){
        motor1.encoderTick();
        motor2.encoderTock();
    }

    TEST_ASSERT_EQUAL_INT(1000, motor1.getEncoder());
    TEST_ASSERT_EQUAL_INT(-1000, motor2.getEncoder());

    for (int i = 0; i < 2000; i++){
        motor1.encoderTock();
        motor2.encoderTick();
    }

    TEST_ASSERT_EQUAL_INT(-1000, motor1.getEncoder());
    TEST_ASSERT_EQUAL_INT(1000, motor2.getEncoder());

    motor1.clearEncoder();
    motor2.clearEncoder();

    TEST_ASSERT_EQUAL_INT(0, motor1.getEncoder());
    TEST_ASSERT_EQUAL_INT(0, motor2.getEncoder());
}



#endif