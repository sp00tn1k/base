#ifdef UNIT_TEST

#include <unity.h>

#include "sp00tn1k-planner-test.hpp"
// #include "sp00tn1k-motor-test.hpp"
// #include "sp00tn1k-battery-test.hpp"

int main(int argc, char** argv) {
    UNITY_BEGIN();

    RUN_TEST(FirstPointCurrentSpeedInRange);
    // RUN_TEST(LastPointCurrentSpeedInRange);
    // RUN_TEST(PositiveSlope);
    // RUN_TEST(NegativeSlope);
    // RUN_TEST(DeletePlan);

    // RUN_TEST(CreateMotors);
    // RUN_TEST(CheckEncoders);

    // RUN_TEST(CheckSoCCalculation);

    return UNITY_END();
}

#endif