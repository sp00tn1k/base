#ifdef UNIT_TEST

#include <unity.h>

#include "sp00tn1k-battery.hpp"
#include "sp00tn1k-hardware.h"

void CheckSoCCalculation(){
    
    Battery b;

    for (double i = 0.0; i < 15.0; i += 0.1) {
        std::cout << b.getSoC(i);
    }

        TEST_ASSERT_EQUAL_INT(0, 0);

}

#endif