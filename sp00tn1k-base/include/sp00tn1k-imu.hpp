#ifndef SP00TN1K_IMU_H
#define SP00TN1K_IMU_H

#include "BNO055_support.h"		//Contains the bridge code between the API and Arduino
#include <Wire.h>

#include <ros.h>
#include <sensor_msgs/Imu.h>

extern ros::NodeHandle nh;

class IMU {

    sensor_msgs::Imu _imu_msg;
    ros::Publisher *_imu_pub;

    const char _topic[4] = "imu";

    const unsigned int _update_rate = 200; //ms
    unsigned long _now = 0;

    struct bno055_t myBNO;

   public:
    
    IMU() {

        Wire.begin();

        // //Initialization of the BNO055
        // // this is not working :(
        // BNO_Init(&myBNO); //Assigning the structure to hold information about the device

        // //Configuration to NDoF mode
        // bno055_set_operation_mode(OPERATION_MODE_NDOF);

        // this->_imu_msg.header.seq = 0;
        // this->_imu_msg.header.frame_id = "drive_base";

        // this->_imu_pub = new ros::Publisher(this->_topic, &_imu_msg);
        // nh.advertise(*(this->_imu_pub));

        _now = millis();


        // TODO: Big todo: calibrate the sensor or load calibration data


    }

    void update() {
        
        if (millis() > _now + _update_rate) {

            bno055_quaternion q;
            bno055_read_quaternion_wxyz(&q);
            this->_imu_msg.orientation.w = (float)q.w;
            this->_imu_msg.orientation.x = (float)q.x;
            this->_imu_msg.orientation.y = (float)q.y;
            this->_imu_msg.orientation.z = (float)q.z;

            bno055_accel a;
            bno055_read_accel_xyz(&a);
            this->_imu_msg.linear_acceleration.x = a.x;
            this->_imu_msg.linear_acceleration.y = a.y;
            this->_imu_msg.linear_acceleration.z = a.z;

            bno055_gyro g;
            bno055_read_gyro_xyz(&g);
            this->_imu_msg.angular_velocity.x = (float)g.x * PI / 180.0;
            this->_imu_msg.angular_velocity.y = (float)g.y * PI / 180.0;
            this->_imu_msg.angular_velocity.z = (float)g.z * PI / 180.0;

            this->_imu_msg.header.stamp = nh.now();
            this->_imu_pub->publish(&(this->_imu_msg));

            this->_imu_msg.header.seq++;

            _now = millis();

        }


    }
};

#endif /* SP00TN1K_IMU_H */