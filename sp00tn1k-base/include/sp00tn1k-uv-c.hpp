#ifndef UVC
#define UVC

#include <ros.h>
#include <std_msgs/Bool.h>

#include "sp00tn1k-hardware.h"

// NodeHandle must be declared in the main file
extern ros::NodeHandle nh;

class UVCLamp {
   private:
    const char* ROS_TOPIC_UVCLAMP = "uv_active";

   public:
    ros::Subscriber<std_msgs::Bool, UVCLamp> lamp_sub;

    void lamp_cmd(const std_msgs::Bool& msg) {
        if (msg.data) {
            digitalWrite(UVC_LAMP_1, HIGH);
            delay(250);
            digitalWrite(UVC_LAMP_2, HIGH);
            delay(250);
            digitalWrite(UVC_LAMP_3, HIGH);
            delay(250);
            digitalWrite(UVC_LAMP_4, HIGH);
        } else {
            digitalWrite(UVC_LAMP_1, LOW);
            digitalWrite(UVC_LAMP_2, LOW);
            digitalWrite(UVC_LAMP_3, LOW);
            digitalWrite(UVC_LAMP_4, LOW);
        }
    }

    UVCLamp() : lamp_sub(ROS_TOPIC_UVCLAMP, &UVCLamp::lamp_cmd, this) {
        pinMode(UVC_LAMP_1, OUTPUT);
        pinMode(UVC_LAMP_2, OUTPUT);
        pinMode(UVC_LAMP_3, OUTPUT);
        pinMode(UVC_LAMP_4, OUTPUT);
        digitalWrite(UVC_LAMP_1, LOW);
        digitalWrite(UVC_LAMP_2, LOW);
        digitalWrite(UVC_LAMP_3, LOW);
        digitalWrite(UVC_LAMP_4, LOW);
        nh.subscribe(lamp_sub);
    }

    ~UVCLamp() {
        digitalWrite(UVC_LAMP_1, LOW);
        digitalWrite(UVC_LAMP_2, LOW);
        digitalWrite(UVC_LAMP_3, LOW);
        digitalWrite(UVC_LAMP_4, LOW);
        pinMode(UVC_LAMP_1, INPUT_PULLUP);
        pinMode(UVC_LAMP_2, INPUT_PULLUP);
        pinMode(UVC_LAMP_3, INPUT_PULLUP);
        pinMode(UVC_LAMP_4, INPUT_PULLUP);
    }
};

#endif  //UVC