#ifndef SP00TN1K_MOTOR_H
#define SP00TN1K_MOTOR_H

#include <CytronMotorDriver.h>
// #include <PID_v1.h>
#include "PID_IC.h"

#ifdef DEV_BUILD
    #include <ros.h>
    #include <std_msgs/Float32MultiArray.h>
#endif

#include "sp00tn1k-planner.hpp"

#ifdef DEV_BUILD
    extern ros::NodeHandle nh;
#endif

/*
 * all timing is in micro-seconds, unless otherwise specified
 */

class Motor : public CytronMD {
   protected:
    // motor data https://www.robotshop.com/en/12v-313rpm-4166oz-in-hd-premium-planetary-gearmotor-encoder.html
    // Driving motor gear ratio
    const uint8_t GEAR_RATIO = 27;

    // Encoder has a 24 pole magnet.
    // We are only counting rising edges, so 12 ticks
    const uint8_t PPR = 12;

    // Data for loal motion planning
    const int16_t MAX_RPM = 313;
    // ideally, I'd not want tit to be static, but this var is used also for buffer array declaration...
    static const uint8_t RPM_RANGE_STEPS = 32;
    const uint32_t RPM_RANGE_TIME = 12 * 1000000;

    // PID should be updated at least N times on each S-curve step, milliseconds
    uint32_t pidUpdateTimestep = 100000;  //(RPM_RANGE_TIME / RPM_RANGE_STEPS) / (10 * 1000);

    // Resolution of the PWM
    const uint8_t PWM_RESOLUTION = 255;

    // Min/Max PID value
    const uint8_t PID_MAX = 128;

    // actual and target RPMs of the motor
    float measuredRPM, targetRPM;
    // power set by PID based on actual and target accelerations, in PWM units
    // double setPower;
    float setPower;

    bool driving = false;

    // PID parameters
    // double Kp = 0.5, Ki = 3.5, Kd = 0.0125;
    float Kp = 0.0f, Ki = 20.0f, Kd = 0.15f;
    //derivative filter constant D(s) = s / (1 + s / N)
    //a good rule is: N > (10 * Kd / Kp) (also avoid too large values)
    float N = 10.0f;
    
    // encoder pins and counter
    uint8_t enc_pwr;
    uint8_t enc_a;
    uint8_t enc_b;
    volatile long encoderCounter = 0;
    static const uint8_t ENC_WINDOW_SIZE = 4;
    long encoderWindow[ENC_WINDOW_SIZE];
    uint8_t encoderWindowIdx = 0;

    // MotionPlanner is responsible for generating smooth accelleration/deceleration profile
    MotionPlanner planner = MotionPlanner(-1 * MAX_RPM, MAX_RPM, RPM_RANGE_STEPS, RPM_RANGE_TIME);

    // current plan
    Plan* plan = nullptr;
    PlanPoint* point = nullptr;

    // timing parameters, in s
    unsigned long encoderLastUpdTime = 0;
    const unsigned long encoderUpdTimestep = pidUpdateTimestep; // 50000;

    unsigned long motorLastUpdTime = 0;

    // fail counter/timeout is used to reject plan which cannot be executed
    int failCounter = 0;
    const unsigned long failPlanRejectLimit = 3 * 1000000;  // in seconds

    // velocity controller
    // Note: acceleration controller does not seem to be possible because of noisy accekeration measurements
    // PID pid = PID(&measuredRPM, &setPower, &targetRPM, Kp, Ki, Kd, P_ON_E, DIRECT);
    PID_IC pid = PID_IC(&setPower, Kp, Ki, Kd, 1.0f, 10);

    // name (ID) of this motor
    char* motorName;

    bool ready = false;

// only needed for development time builds
#ifdef DEV_BUILD

    // send on every encoder update?
    // motorDataMsg: measuredRPM, targetRPM, setPower, encoderWindows[encoderWindowIdx]
    std_msgs::Float32MultiArray motorDataMsg;
    ros::Publisher* motorDataPub;

    // send on every new plan
    double motionPlanDat[RPM_RANGE_STEPS];
    std_msgs::Float32MultiArray motionPlanMsg;
    ros::Publisher* motionPlanPub;

    // vars for topic names
    char res[20];
    const char* resp = &res[0];

#endif

   public:
    // Adjust PID parameters
    void setPIDParams(double p, double i, double d) {
        Kp = p;
        Ki = i;
        Kd = d;
        // pid.SetTunings(Kp, Ki, Kd);  // inherited from PID
        pid.SetTunings(Kp, Ki, Kd, 1.0f);
    }

    // setters to increment/decrement encoder
    void encoderTick() { this->encoderCounter++; }
    void encoderTock() { this->encoderCounter--; }

    // getter for encoder counter
    const long getEncoder() { return this->encoderCounter; }
    // clear encoder
    void clearEncoder() { this->encoderCounter = 0; }

    // get motor data
    const double getRPM() { return measuredRPM; }

    // get name
    const char* getName() {
        return motorName;
    }

    // if motor is ready
    bool isReady() {
        return this->ready;
    }

    // set new target speed for the motor
    bool setTargetRPM(double value) {

        double rpm = value;
        
        if (rpm > MAX_RPM) {
            rpm = MAX_RPM;
        } else if (rpm < -1 * MAX_RPM) {
            rpm = -1 * MAX_RPM;
        }

        // only if the requested RPM is realistic
        if ( this->isReady()) {

            // BYPASS PLANNER
            ///////////////////////////////////////////////////////////////////////////////////

            //pid.SetMode(AUTOMATIC);
            // pid.Reset();
            targetRPM = rpm;
            driving = true;
            return true;

            //////////////////////////////////////////////////////////////////////////////////

            // pid.Reset();

            // clear current point
            delete this->point;
            this->point = nullptr;

            // create a new plan for rpm
            // clear the current plan
            this->plan->clear();

            // TODO: what if this fails?
            planner.fillPlan(this->plan, this->measuredRPM, rpm);
// TODO: think about acceleration profile smoothing?

// log th plan to ros
#ifdef DEV_BUILD

            motorDataMsg.data[0] = measuredRPM;
            motorDataMsg.data[1] = rpm;
            motionPlanMsg.layout.dim_length = 1;
            motionPlanMsg.data_length = 2;
            motionPlanPub->publish(&motorDataMsg);


            if (!plan->isEmpty()){
                int i = 0;
                plan->reset();
                do{
                    motionPlanDat[i] = plan->at()->value;
                    i++;
                } while (plan->next() != nullptr);
                motionPlanMsg.data_length = i;
                motionPlanPub->publish(&motorDataMsg);
            } else {
                nh.logwarn("plan empty");
            }

#endif

            // if generated plan contains something, assign the first point
            if (!plan->isEmpty()) {
                point = this->plan->pop();
                // not ready until we execute the first point
                this->ready = false;
            }

            motorLastUpdTime = micros();
            driving = true;
            // nh.loginfo("[ :-) ] Setting motor RPM.");
            return true;
        } else {
            // nh.loginfo("[ :-( ] Cannot set motor RPM!");
            driving = false;
            return false;
        }
    }

    // time in sec!
    int update() {
        // get current timestamp, sec
        unsigned long now = micros();

        // update current RPM and acceleration data
        if ((now - encoderLastUpdTime) > encoderUpdTimestep) {
            encoderWindow[encoderWindowIdx] = encoderCounter;
            (encoderWindowIdx >= ENC_WINDOW_SIZE - 1) ? encoderWindowIdx = 0 : encoderWindowIdx++;

            long sum = 0;
            for (uint8_t i = 0; i < ENC_WINDOW_SIZE; i++) {
                sum += encoderWindow[i];
            }

            //measuredRPM = 60.0 * ((((double)(sum) / ENC_WINDOW_SIZE) / ((double)(now - encoderLastUpdTime) / 1000000)) / PPR) / GEAR_RATIO;  // in RPM (revolutions/minute)
            measuredRPM = 60.0f * ((encoderCounter / ((double)(now - encoderLastUpdTime) / 1000000.0f)) / PPR) / GEAR_RATIO;  // in RPM (revolutions/minute)
            encoderLastUpdTime = now;
            clearEncoder();

            // Update PID
            if (driving == true) {
                float error = targetRPM - measuredRPM;
                pid.AutoCompute(error);
                setSpeed(setPower);
                // setSpeed(targetRPM * 0.84f);

            } else {
                pid.Reset();
            }
        }

        // Update motor target rpm based on the plan.
        // The logic here is to 1) check if there is a point to set next,
        // then 2) if it's time to set it, 3) check if we have actually reached our previous target.
        // And only then set a new target. If the target is not yet reached, then increase the fail counter.
        // After N consequetive fails, reject the plan. N is a

        if (this->point != nullptr) {
            if ((now - motorLastUpdTime) > point->timestep) {
                //start the pid
                //pid.SetMode(AUTOMATIC);
                // pid.Reset();

                // Make sure the current RPM is in the range 90%-110% of what we were supposed to achieve.
                // if ((abs(measuredRPM) > (0.75 * abs(targetRPM))) && (abs(measuredRPM) < (1.25 * abs(targetRPM)))) {
                if (true) {
                    // extract plan value to the next targetRPM
                    targetRPM = point->value;

                    // because the c++ standard does not specify what happens to the pointer here,
                    // set it explicitly to nullptr to avoid making dangling pointer here...
                    delete point;
                    point = nullptr;

                    if (!plan->isEmpty()) {
                        point = plan->pop();
                    }
                    // TODO: Maybe enforse PID update after new plan point?
                    motorLastUpdTime = now;
                    failCounter = 0;

                    this->ready = true;
                    // nh.loginfo("setting speed...");

                } else {
                    failCounter++;
                }
            }
        } else {
            // TODO: Test on the robot if that makes any power consumption difference
            if ((abs(targetRPM) < 0.1) && (abs(measuredRPM) < 0.1)) {
                // power off the motor if not moving and there is no plan
                setPower = 0.0;
                setSpeed(setPower);
                pid.Reset();
                driving = false;
                // pid.SetMode(MANUAL);
            }
        }

        // reject the current plan if fail counter indicates that we cannot reach it
        if ((failCounter > 0) && ((now - motorLastUpdTime) > failPlanRejectLimit)) {
            setPower = 0.0;
            driving = false;
            pid.Reset();
            setSpeed(setPower);
            // pid.SetMode(MANUAL);
            if (plan != nullptr) {
                delete plan;
                plan = nullptr;
            }
            if (point != nullptr) {
                delete point;
                point = nullptr;
            }
            // nh.logwarn(motorName + " plan rejected!");
            nh.logwarn("Plan rejected!");
        }

        // // compute PID params
        // if (pid.Compute()) {
        //     // inherited from Cytron lib
        //     setSpeed(setPower);
        //     // setSpeed(targetRPM * 0.84f);  // for testing only, as RPM units have no relation to actual PWM value
        // }


        return 0;
    }

    // Configure the motor driver.
    // Motor(NAME, MODE, PWM, DIR, ENC_PWR, ENCA, ENCB) // ENCA must be external interrupt
    Motor(const char* name, MODE m, uint8_t p1, uint8_t p2, uint8_t encpwr, uint8_t enca, uint8_t encb) : CytronMD(m, p1, p2),
                                                                                                          measuredRPM(0.0),
                                                                                                          targetRPM(0.0),
                                                                                                          setPower(0.0),
                                                                                                          enc_pwr(encpwr),
                                                                                                          enc_a(enca),
                                                                                                          enc_b(encb) {
        // set interrupt for encoder control
        pinMode(enc_a, INPUT_PULLUP);
        pinMode(enc_b, INPUT_PULLUP);

        // set encoder power to high
        pinMode(enc_pwr, OUTPUT);
        digitalWrite(enc_pwr, HIGH);

        // aasign name to the motor, cast to char*
        motorName = (char*)name;

        plan = new Plan();


#ifdef DEV_BUILD
        // create a publisher for motor diagnostic data
        // strcpy(res, name);
        // strcat(res, "_data");
        // motorDataPub = new ros::Publisher(resp, &motorDataMsg);
        // nh.advertise(*motorDataPub);

        // publisher for planner
        strcpy(res, name);
        strcat(res, "_plan");
        motionPlanPub = new ros::Publisher(resp, &motionPlanMsg);
        nh.advertise(*motionPlanPub);
        motionPlanMsg.data = (float*)motionPlanDat;
#endif

        // setting PID parameters
        // pid.SetOutputLimits(-1 * PID_MAX, PID_MAX);
        // pid.SetSampleTime(pidUpdateTimestep);
        pid.SetSaturation(-1 * PID_MAX, PID_MAX);

        // set mode to manual until a command is received
        // pid.SetMode(MANUAL);
        pid.Reset();

        // ready to receive requests
        this->ready = true;
    }
    virtual ~Motor() {
        // set encoder power to low
        digitalWrite(enc_pwr, LOW);

        delete plan;

#ifdef DEV_BUILD
        // why th hck there is no unadvertize function is rosserial?!?!?!
        delete motorDataPub;
        delete motionPlanPub;
#endif
    }
};

#endif /* SP00TN1K_MOTOR_H */