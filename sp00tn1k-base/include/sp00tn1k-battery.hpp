#ifndef _SP00TN1K_BATTERY_H_
#define _SP00TN1K_BATTERY_H_

#include <ros.h>
#include <sensor_msgs/BatteryState.h>

// Battery constants for sp00tn1k-4 ( Shido LTZ12S )
// http://www.gobatteries.co.uk/wp-content/uploads/2018/05/Shido-Datasheet-FAQs.pdf


#define BATTERY_VOLTAGE A0   // not known yet
#define BATTERY_CURRENT A11  // not sure yet
#define ADC_RESOLUTION 1024
#define MV_PER_AMP 100  // use 100 for 20A Module and 66 for 30A Module
#define AC_OFFSET 2500

class Battery {
   private:
    const int CURRENT_SENSOR = A1;
    const int VOLTAGE_SENSOR = A2;

    const double CHARGER_VOLTAGE = 14.7; //TODO: Verify
    const double BATTERY_CAPACITY = 2 * 5.0;
    const double ADC_REF = 5.0;
    // this are values for sp00tn1k-3
    const double VOLTAGE_DIVIDER = 10.4; //TODO: Verify

    const double VOLTAGES[11] = {12.5, 12.73, 12.87, 13.00, 13.10, 13.12, 13.13, 13.16, 13.27, 13.30, 14.34};
    //                             0%    10%    20%    30%    40%    50%    60%    70%    80%    90%   100%
   
    const unsigned int BATTERY_UPDATE_TIME = 1000;

    const char topic[8] = "battery";

    unsigned long lastBatteryUpdate = 0;
    // Published battery status
    sensor_msgs::BatteryState batteryStateMsg;
    ros::Publisher *batteryPublisher;

    

   public:
    /**
     * Init battery
     */
    Battery(ros::NodeHandle &nh, int currentSensorPin, int voltageSensorPin);

    double getSoC(double v);

    void update();
};

#endif /* _SP00TN1K_BATTERY_H_ */