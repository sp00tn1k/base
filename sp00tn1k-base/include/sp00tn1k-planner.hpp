/**
 * Rudimentary motion planner on top of the motor controller to implement S curve velocity profile.
 * Based on smootherstep algorithm https://en.wikipedia.org/wiki/Smoothstep
 */

#ifndef SP00TN1K_PLANNER_H
#define SP00TN1K_PLANNER_H

#include <math.h>

// PlanPoint consists of a unsigned long timestep from previous point (0 for the first) and a double value
struct PlanPoint {
    unsigned long timestep;
    double value;
    PlanPoint(unsigned long timestep, double value) : timestep(timestep), value(value) {}
    virtual ~PlanPoint() {}
    PlanPoint(PlanPoint& other) = delete;
    PlanPoint& operator=(PlanPoint& other) = delete;
    PlanPoint(PlanPoint&& other) = delete;
    PlanPoint& operator=(PlanPoint&& other) = delete;
};

// PlanNode is a node for a queue structure
struct PlanNode {
    PlanPoint* point = nullptr;
    PlanNode* next = nullptr;
    PlanNode(PlanPoint* point) : point(point) {}
    virtual ~PlanNode() {
        point = nullptr;
        next = nullptr;
    }
    PlanNode(PlanNode& other) = delete;
    PlanNode& operator=(PlanNode& other) = delete;
    PlanNode(PlanNode&& other) = delete;
    PlanNode& operator=(PlanNode&& other) = delete;
};

// Plan is a queue structure that holds PlanPoints in PlanNodes
// Access using push() and pop()
class Plan {
   private:
    PlanNode *_head = nullptr, *_tail = nullptr, *_at = nullptr;

   public:
    // check if the plan is empty
    inline bool isEmpty() {
        return (_head == nullptr);
    }

    int length() {
        int counter = 1;
        PlanNode* next = nullptr;

        if (this->isEmpty()) {
            return 0;
        }

        next = _head;

        while (next->next != nullptr) {
            counter++;
            next = next->next;
        }
        return counter;
    }

    // push a point to the end of the plan
    inline void push(PlanPoint* p) {
        PlanNode* n = new PlanNode(p);
        if (_head != nullptr) {
            _tail->next = n;
            _tail = n;
        } else {
            _head = _tail = _at = n;
        }
    }
    // pull a point from the beginning
    inline PlanPoint* pop() {
        if (_head != nullptr) {
            PlanNode* n = _head;
            if (_head == _tail) {
                _head = _tail = _at = nullptr;
            } else {
                if( _at == _head){
                    _at = _head->next;
                }
                _head = _head->next;
            }
            // PlanPoint* p = new PlanPoint(*(n->point));
            PlanPoint* p = n->point;
            delete n;
            n = nullptr;
            return p;
        } else {
            return nullptr;
        }
    }
    // read next point without removal
    // returns nullptr if no next exist
    inline PlanPoint* next(){
        if (_at != nullptr) {
            PlanNode* n = _at;
            _at = _at->next;
            return n->point;
        } else {
            return nullptr;
        }
    }
    inline PlanPoint* at(){
        if (_at != nullptr) {
            return _at->point;
        } else {
            return nullptr;
        }
    }
    // reset iterator
    inline void reset(){
        _at = _head;
    }
    // clear the list
    void clear() {
        // pull nodes until get a nullptr
        PlanPoint* p = nullptr;
        while ((p = this->pop()) != nullptr) {
            delete p;
            p = nullptr;
        }
    }
    // make a new plan with an initial point
    Plan(PlanPoint* p) {
        PlanNode* n = new PlanNode(p);
        _head = _tail = _at = n;
    }

    Plan() {
        _head = _tail = _at = nullptr;
    }

    Plan(Plan& other) = delete;
    Plan& operator=(Plan& other) = delete;
    Plan(Plan&& other) = delete;
    Plan& operator=(Plan&& other) = delete;

    // delete plan by droppping all nodes
    virtual ~Plan() {
        this->clear();
    }
};

class MotionPlanner {
    // min and max speeds, RPMs
    double VALUE_MIN = -300.0f;
    double VALUE_MAX = 300.0f;
    // minimum time which the speed change from full forward to full backward should take, seconds
    // steps in the full span change
    uint8_t FULL_SPAN_STEPS = 64;
    uint32_t FULL_SPAN_TIME = 8 * 1000000;

    uint32_t STEP_TIME = FULL_SPAN_TIME / FULL_SPAN_STEPS;

    // ken perlin, smootherstep
    inline static double smootherstep(double edge_left, double edge_right, double x) {
        // Scale, and clamp x to 0..1 range
        x = clamp((x - edge_left) / (edge_right - edge_left), 0.0f, 1.0f);
        // Evaluate polynomial
        return x * x * x * (x * (x * 6.0f - 15.0f) + 10.0f);
    }

    inline static double clamp(double x, double lower_limit, double upper_limit) {
        if (x < lower_limit)
            return lower_limit;
        if (x > upper_limit)
            return upper_limit;
        return x;
    }

   public:
   
    MotionPlanner(double min, double max, uint8_t steps, uint32_t time) : VALUE_MIN(min),
                                                                       VALUE_MAX(max),
                                                                       FULL_SPAN_STEPS(steps),
                                                                       FULL_SPAN_TIME(time) {
        // Nothing to do here yet
    }

    void fillPlan(Plan* plan, double currentValue, double targetValue) {
        // make sure the speeds are within the range
        double cSpeed = clamp(currentValue, VALUE_MIN, VALUE_MAX);
        double tSpeed = clamp(targetValue, VALUE_MIN, VALUE_MAX);

        double deltaSpeed = tSpeed - cSpeed;
        int steps = ceil(fabs(deltaSpeed) * FULL_SPAN_STEPS / (VALUE_MAX - VALUE_MIN));

        // first point is our current speed
        // plan->push(new PlanPoint(0, cSpeed));

        if (deltaSpeed >= 0) {
            for (int i = 1; i <= steps; i++) {
                double s = smootherstep(0.0f, 1.0f, (double)i / steps);
                if(plan->isEmpty()){
                    plan->push(new PlanPoint(0.0, cSpeed + deltaSpeed * s));
                } else {
                    plan->push(new PlanPoint(STEP_TIME, cSpeed + deltaSpeed * s));
                }
            }
        } else {
            for (int i = steps - 1; i >= 0; i--) {
                double s = smootherstep(0.0f, 1.0f, (double)i / steps);
                if(plan->isEmpty()){
                    plan->push(new PlanPoint(0.0, tSpeed + fabs(deltaSpeed) * s));
                } else {
                    plan->push(new PlanPoint(STEP_TIME, tSpeed + fabs(deltaSpeed) * s));
                }
            }
        }
    }
};

#endif /* SP00TN1K_PLANNER_H */