#ifndef SP00TN1K_DRIVE_BASE_H
#define SP00TN1K_DRIVE_BASE_H

#include <geometry_msgs/Twist.h>
#include <std_msgs/Float32MultiArray.h>
#include <ros.h>
#include <ros/time.h>

#include "sp00tn1k-hardware.h"
#include "sp00tn1k-motor.hpp"

// NodeHandle must be declared in the main file
extern ros::NodeHandle nh;

class DriveBase {
   protected:    

    // Topics
    const char* ROS_TOPIC_VELOCITY = "velocity";
    const char* ROS_TOPIC_CMD_VEL = "cmd_vel";
    // Names of motors
    const char* MOTOR_NAME_LEFT = "left";
    const char* MOTOR_NAME_RIGHT = "right";

    // sp00tn1k-iv
    const double WHEEL_RADIUS = 0.105;    // sp00tn1k-iv flexie wheels
    const double WHEEL_SEPARATION = 0.5;  // sp00tn1k-iv flexie wheels
    const double WHEELBASE_RADIUS = (WHEEL_SEPARATION / 2);
    const double WHEEL_CIRCUMFERENCE = (2 * PI * WHEEL_RADIUS);

    const unsigned int _update_rate = 200; //ms
    unsigned long _now = 0;

    // last requested values are needed to filter out messages requesting the same velocities
    double _lastVelLinear = 0.0;
    double _lastVelAngular = 0.0;

    // subscriber for twist
    // ros::Subscriber<geometry_msgs::Twist> * twist_sub;//(this->getDrivingTopic(), &twistMessageCallback);

    // Published twist based on encoders
    geometry_msgs::Twist currentTwistMsg;

    // Motors' raw data
    // std_msgs::Float32MultiArray motorsDataMsg;

    // std_msgs::Float32MultiArray motorsPIDMsg;
    // ros::Subscriber<std_msgs::Float32MultiArray>* motorsPIDSub;

    void publishCurrentTwist() {
        double m1Speed = motor1.getRPM() * WHEEL_CIRCUMFERENCE / 60.0;
        double m2Speed = motor2.getRPM() * WHEEL_CIRCUMFERENCE / 60.0;

        currentTwistMsg.linear.x = (m2Speed + m1Speed) / 2;
        currentTwistMsg.angular.z = (m1Speed - m2Speed) / WHEEL_SEPARATION;

        //currentTwistMsg.linear.y = m1Speed;
        //currentTwistMsg.linear.z = m2Speed;
        

        currentTwistPublisher.publish(&currentTwistMsg);
    }

    // void publishMotorsData() {
    //     motorsDataMsg.data[0] = motor1.getRPM();
    //     motorsDataMsg.data[1] = motor2.getRPM();
    //     motorsDataPub.publish(&motorsDataMsg);
    // }

    // TODO: tweak the epsilon numbers to the reasonable values
    inline bool sameVelLinear(const double a, const double b) {
        return fabs(a - b) < 0.005;
    }
    inline bool sameVelAngular(const double a, const double b) {
        return fabs(a - b) < 0.001;
    }


    // message timeout
    unsigned long lastTwistTime = 0;
    const unsigned long twistMsgTimeout = 1000;

   public:

    ros::Publisher currentTwistPublisher = ros::Publisher(ROS_TOPIC_VELOCITY, &currentTwistMsg);
    // ros::Publisher motorsDataPub = ros::Publisher("motors", &motorsDataMsg);

    // motors
    Motor motor1 = Motor(MOTOR_NAME_RIGHT, PWM_DIR, MOTOR_A1_PWM, MOTOR_A1_DIR, ENC_A1_PWR, ENC_A1_A, ENC_A1_B);  // MOTOR1, assumed RIGHT
    Motor motor2 = Motor(MOTOR_NAME_LEFT, PWM_DIR, MOTOR_A2_PWM, MOTOR_A2_DIR, ENC_A2_PWR, ENC_A2_A, ENC_A2_B);  // MOTOR2, assumed LEFT

    void motorsPIDCallback(const std_msgs::Float32MultiArray &msg){
        char result[8];
        dtostrf(msg.data[0], 2, 3, result);
        nh.loginfo(result);
        motor1.setPIDParams(msg.data[0], msg.data[1], msg.data[2]);
        motor2.setPIDParams(msg.data[0], msg.data[1], msg.data[2]);
    }

    // Twist message callback. Calculate target wheels' speeds from twist message, m/s.
    void twistMessageCallback(const geometry_msgs::Twist& msg) {
        lastTwistTime = millis();

        // if this message requests the same velocities as before, then just ignore it
        if ( sameVelLinear( _lastVelLinear, msg.linear.x ) && sameVelAngular( _lastVelAngular, msg.angular.z ) ){
            // nh.loginfo("[ :-/ ] Same last target velocities.");
            return;
        }

        // and if we are already spinning the wheels with desired velocities, skip the message as well
        if ( sameVelLinear( currentTwistMsg.linear.x, msg.linear.x ) && sameVelAngular( currentTwistMsg.angular.z, msg.angular.z ) ){
            // nh.loginfo("[ :-/ ] Same current and target velocities.");
            return;
        }

        _lastVelLinear = msg.linear.x;
        _lastVelAngular = msg.angular.z;

        // decompose twist to individual motor target speeds
        double motor1targetSpeed = msg.linear.x + (msg.angular.z * WHEELBASE_RADIUS);
        double motor2targetSpeed = msg.linear.x - (msg.angular.z * WHEELBASE_RADIUS);

        if (motor1.setTargetRPM(60.0 * motor1targetSpeed / WHEEL_CIRCUMFERENCE)) {
            // nh.loginfo("[ :-) ] Setting target RPM.");
        } else {
            nh.logwarn("[base] Cannot set motor1 target RPM!");
        }

        if (motor2.setTargetRPM(60.0 * motor2targetSpeed / WHEEL_CIRCUMFERENCE)) {
            // nh.loginfo("[ :-) ] Setting target RPM.");
        } else {
            nh.logwarn("[base] Cannot set motor2 target RPM!");
        }

        
    }

    const char* getDrivingTopic() {
        return this->ROS_TOPIC_CMD_VEL;
    }

    // update drive base state based on current delta time
    void update() {

        // publish twist
        if (millis() > _now + _update_rate) {        
            publishCurrentTwist();
            // publishMotorsData();
            _now = millis();
            
            // FOR DEBUGGING ONLY!
            // motor1.setTargetRPM((char)_now);
        }

        if (millis() > lastTwistTime + twistMsgTimeout ){
            // this is not going to work with the single message driving, so disable it for the moment for debugging
            // motor1.setTargetRPM(0);
            // motor2.setTargetRPM(0);
        }

        // update motors
        motor1.update();
        motor2.update();
    }

    DriveBase() {

        // set and advertise current twist
        // this->currentTwistPublisher = new ros::Publisher(ROS_TOPIC_VELOCITY, &currentTwistMsg);
        nh.advertise(this->currentTwistPublisher);
        // nh.advertise(this->motorsDataPub);

        // motorsDataMsg.data_length = 2;
        // motorsDataMsg.data = (float *)malloc(sizeof(double) * 2);

        // motorsPIDSub = new ros::Subscriber<std_msgs::Float32MultiArray>("motors_pid", &motorsPIDCallback);

        // motor1.setTargetRPM(10);

        lastTwistTime = millis();
    }
    virtual ~DriveBase() {}
};

#endif /* SP00TN1K_DRIVE_BASE_H */