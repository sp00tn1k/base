#include "sp00tn1k-battery.hpp"

Battery::Battery(ros::NodeHandle &nh, int currentSensorPin, int voltageSensorPin):CURRENT_SENSOR(currentSensorPin),VOLTAGE_SENSOR(voltageSensorPin) {

    pinMode(BATTERY_VOLTAGE, INPUT);
    pinMode(BATTERY_CURRENT, INPUT);

    this->batteryPublisher = new ros::Publisher(this->topic, &this->batteryStateMsg);

    nh.advertise(*(this->batteryPublisher));

    this->batteryStateMsg.power_supply_technology = 0;  // there is no value for VRLA
    this->batteryStateMsg.capacity = this->BATTERY_CAPACITY;
    this->batteryStateMsg.design_capacity = this->BATTERY_CAPACITY;
    this->batteryStateMsg.charge = 0;
    this->batteryStateMsg.percentage = 0.0;
}

double Battery::getSoC(double voltage) {
    int soc = 0;
    int socIndex = 0;
    for (int i = sizeof(this->VOLTAGES) / sizeof(this->VOLTAGES[0]) - 1; i > 0; i--) {
        if (voltage > this->VOLTAGES[i]) {
            socIndex = i;
            break;
        }
    }
    soc = (sizeof(this->VOLTAGES) / sizeof(this->VOLTAGES[0])) * socIndex;
    if (socIndex < sizeof(this->VOLTAGES) / sizeof(this->VOLTAGES[0]) - 1) {
        // add a linearly interpolated value
        soc += (sizeof(this->VOLTAGES) / sizeof(this->VOLTAGES[0])) * ((this->VOLTAGES[socIndex + 1] - this->VOLTAGES[socIndex]) / (voltage - this->VOLTAGES[socIndex]));
    }
}

void Battery::update() {
    unsigned long now = millis();
    unsigned long deltaTime = now - this->lastBatteryUpdate;
    if (deltaTime > this->BATTERY_UPDATE_TIME) {
        
        // read data from the current sensor for state of charge estimation
        int currentSensorReading = analogRead(CURRENT_SENSOR);
        double currentSensorValue = (currentSensorReading / 1023.0) * 5000;
        double convertedCurrent = (currentSensorValue - AC_OFFSET) / MV_PER_AMP;

        double voltage = this->VOLTAGE_DIVIDER * analogRead(BATTERY_VOLTAGE) * this->ADC_REF / ADC_RESOLUTION;
        double current = 0.185 * (2.5 + analogRead(BATTERY_CURRENT) * this->ADC_REF / ADC_RESOLUTION);

        if (voltage > this->CHARGER_VOLTAGE) {
            this->batteryStateMsg.power_supply_status = 1;
        } else {
            this->batteryStateMsg.power_supply_status = 2;
        }

        // if (voltage > this->VOLTAGE_100) {
        //     this->batteryStateMsg.charge = 1.0 * this->BATTERY_CAPACITY;
        //     this->batteryStateMsg.percentage = 1.0;
        // } else if (voltage > this->VOLTAGE_90) {
        //     this->batteryStateMsg.charge = 0.9 * this->BATTERY_CAPACITY;
        //     this->batteryStateMsg.percentage = 0.9;
        // } else if (voltage > this->VOLTAGE_80) {
        //     this->batteryStateMsg.charge = 0.8 * this->BATTERY_CAPACITY;
        //     this->batteryStateMsg.percentage = 0.8;
        // } else if (voltage > this->VOLTAGE_70) {
        //     this->batteryStateMsg.charge = 0.7 * this->BATTERY_CAPACITY;
        //     this->batteryStateMsg.percentage = 0.7;
        // } else if (voltage > this->VOLTAGE_60) {
        //     this->batteryStateMsg.charge = 0.6 * this->BATTERY_CAPACITY;
        //     this->batteryStateMsg.percentage = 0.6;
        // } else if (voltage > this->VOLTAGE_50) {
        //     this->batteryStateMsg.charge = 0.5 * this->BATTERY_CAPACITY;
        //     this->batteryStateMsg.percentage = 0.5;
        // } else if (voltage > this->VOLTAGE_40) {
        //     this->batteryStateMsg.charge = 0.4 * this->BATTERY_CAPACITY;
        //     this->batteryStateMsg.percentage = 0.4;
        // } else if (voltage > this->VOLTAGE_30) {
        //     this->batteryStateMsg.charge = 0.3 * this->BATTERY_CAPACITY;
        //     this->batteryStateMsg.percentage = 0.3;
        // } else if (voltage > this->VOLTAGE_20) {
        //     this->batteryStateMsg.charge = 0.2 * this->BATTERY_CAPACITY;
        //     this->batteryStateMsg.percentage = 0.2;
        // } else if (voltage > this->VOLTAGE_10) {
        //     this->batteryStateMsg.charge = 0.1 * this->BATTERY_CAPACITY;
        //     this->batteryStateMsg.percentage = 0.1;
        // } else {
        //     this->batteryStateMsg.charge = 0;
        //     this->batteryStateMsg.percentage = 0.0;
        // }

        this->batteryStateMsg.voltage = voltage;
        this->batteryStateMsg.current = current;

        this->batteryPublisher->publish(&this->batteryStateMsg);
        this->lastBatteryUpdate = now;
    }
}
