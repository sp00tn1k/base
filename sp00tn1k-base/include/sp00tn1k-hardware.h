#ifndef _SP00TN1K_HARDWARE_H_
#define _SP00TN1K_HARDWARE_H_

// sp00tn1k-4

// Attention: check the actual board jumpers!

// Right motor and encoder
// Channel 1
#define MOTOR_A1_DIR 8      //
#define MOTOR_A1_PWM 9       //
#define ENC_A1_PWR 51
#define ENC_A1_A 19          //
#define ENC_A1_B 47          //

// Left motor and encoder
// Channel 2
#define MOTOR_A2_DIR 13      //
#define MOTOR_A2_PWM 10      //
#define ENC_A2_PWR 50
#define ENC_A2_A 18          //
#define ENC_A2_B 46          //

// Relay Shield for UV-C lamp
#define UVC_LAMP_1 4
#define UVC_LAMP_2 5
#define UVC_LAMP_3 6
#define UVC_LAMP_4 7

#endif /* _SP00TN1K_HARDWARE_H_ */