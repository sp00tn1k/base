# Arduino Driver for Sp00tn1k Robot

This repo contains sources and documentation for sp00tn1k-robot's differential drive base.

## About

The drive base is supposed to be used on sp00tn1k robot (see https://bitbucket.org/sp00tn1k). When started, it connects to the host computer using rosserial-arduino connector at 500kbps (set corresponding argument in the launch or rusrun call) and listens to twist messages on a cmd_vel topic.

## Current Status

[2020-11-30] Functionality is simplified to a rudimentary PID controller for each motor. PWM frequency at default, thus making annoying noise. The planned local motion planning code is currently bypased due to errors. The battery management is disables to save computational resources. The relay board for lamps is connected to pins 4-7. 

## Future Plans

There were multiple difficulties with using arduino for the base driver. The current plan is to swap the board to Allorium XLR8 to keep compatibility and ease of use of arduino and make use of higher performance and hardware PID, PWM, and quadrature decoder blocks. 

## How to Contribute

Please, create a PR with your code. Test your code on a real board before making a PR. We will do our best to integrate it ASAP upon testing on a reference robot (currently, sp00tn1k-iv).